import { Component, OnInit } from '@angular/core'
import { MapCacheService } from '../../../../core/services/map-cache/map-cache.service'
import { MmpService } from '../../../../core/services/mmp/mmp.service'
import { UtilsService } from '../../../../core/services/utils/utils.service'
import { CachedMapEntry } from '../../../../shared/models/cached-map.model'

@Component({
    selector: 'mindmapp-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

    public cachedMapEntries: CachedMapEntry[]
    public attachedMap: CachedMapEntry

    constructor(private mapCacheService: MapCacheService,
        private mmpService: MmpService,
        private utilsService: UtilsService) {
    }

    public async ngOnInit() {
        console.log('TabsComponent | OnInit')
        this.cachedMapEntries = (await this.mapCacheService.getCachedMapEntries())
            .sort(function (a: CachedMapEntry, b: CachedMapEntry) {
                return b.cachedMap.lastModified - a.cachedMap.lastModified
            })

        console.log('TabsComponent | cachedMapEntries length: ', this.cachedMapEntries.length)

        this.mapCacheService.attachedMap.subscribe((attachedMap: CachedMapEntry) => {
            console.log('TabsComponent | attachedMap', attachedMap)

            console.log('TabsComponent | cachedMapEntries length: ', this.cachedMapEntries.length)

            for (const cachedMapEntry of this.cachedMapEntries) {
                console.log('TabsComponent | attachedMap: ', attachedMap, 'cachedMapEntry: ', cachedMapEntry)
                if (attachedMap !== null && cachedMapEntry && cachedMapEntry.key === attachedMap.key) {
                    cachedMapEntry.cachedMap = attachedMap.cachedMap
                    return
                }
            }

            if (attachedMap !== null) {
                this.cachedMapEntries.unshift(attachedMap)
            }
        })
    }

    public openCachedMap(cachedMapEntry: CachedMapEntry) {
        // if (cachedMapEntry.key !== this.attachedMap.key) {
            this.mmpService.new(cachedMapEntry.cachedMap.data)
            this.mapCacheService.attachMap(cachedMapEntry)
        // }
    }

    public async removeCachedMap(key: string, event: MouseEvent) {
        event.stopPropagation()

        const confirmed = await this.utilsService.confirmDialog('MESSAGES.MAP_DELETION_CONFIRM')

        if (!confirmed) {
            return
        }

        await this.mapCacheService.removeCachedMap(key)

        // Remove the entry from the array.
        this.cachedMapEntries.splice(this.cachedMapEntries.findIndex((cachedMapEntry: CachedMapEntry) => {
            return cachedMapEntry.key === key
        }), 1)

        if (this.attachedMap && this.attachedMap.key === key) {
            // Select the last cached map.
            this.mmpService.new(this.cachedMapEntries[0].cachedMap.data)
            this.mapCacheService.attachMap(this.cachedMapEntries[0])
        }
        // in case last remaining map has been removed, immediatley create a new one
        if (this.cachedMapEntries.length === 0) {
            this.mmpService.new()
            this.mapCacheService.attachNewMap()
        }
    }

}
